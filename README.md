##INSTALLATION

~~~
$ cd /path/to/project
$ git clone git@bitbucket.org:svd222/userapi.git .
$ composer install
$ cp .env.example .env
~~~

###NGINX CONFIGURATION EXAMPLE

~~~
server {
	listen 80;

	
	sendfile        on;
	keepalive_timeout  65;
    	gzip  on;
    	gzip_min_length 1024;
    	gzip_buffers 12 32k;
    	gzip_comp_level 9;
    	gzip_proxied any;
	gzip_types	text/plain application/xml text/css text/js text/xml application/x-javascript text/javascript application/javascript application/json application/xml+rss;
	
	server_name .userapi.local;
	root /var/www/userapi;

	access_log /var/www/userapi/access.log;
	error_log /var/www/userapi/error.log;

	charset utf-8;

	location / {
	    root /var/www/userapi/;
	    index index.php;
	    try_files $uri $uri/ /index.php?$args;
	}

    	location ~ \.php$ {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		
		root /var/www/userapi/;

		try_files $uri $uri/ =404;
		fastcgi_pass unix:/run/php/php7.3-fpm.sock;
		fastcgi_index index.php;
		include fastcgi_params;
		fastcgi_param                   SCRIPT_FILENAME $document_root$fastcgi_script_name;


	    	fastcgi_param QUERY_STRING    $query_string;
	    	fastcgi_param REQUEST_METHOD  $request_method;
	    	fastcgi_param CONTENT_TYPE    $content_type;
	    	fastcgi_param CONTENT_LENGTH  $content_length;
		fastcgi_param  PATH_INFO $fastcgi_path_info;
    	}

}
~~~

next goin to /etc/hosts and add this line 

~~~
127.0.0.1 userapi.local
~~~

~~~
sudo service nginx restart
~~~

after that project should be available at: http://userapi.local

###RUN TESTS

~~~
$ ./vendor/bin/phpunit src/tests
~~~

####SMALL REMARK REGARDING TESTS

On this page https://docs.guzzlephp.org/en/stable/testing.html
in last section named `Test Web Server` 

it is said that for testing purposes you can use a test server, but this is a misconception, since this server is intended only for the library developers themselves
See this issue: https://github.com/guzzle/guzzle/issues/2228
particular, last message:

>>Actually I do not remember why I needed it but I linked the documentation that uses it
http://docs.guzzlephp.org/en/stable/testing.html

>>Feel free to do any action you want, but maybe fix the documentation :)

>It's true that the docs misled me to be able to import GuzzleHttp\Tests\Server

