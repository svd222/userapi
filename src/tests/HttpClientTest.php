<?php
use PHPUnit\Framework\TestCase;
use App\Component\HttpClient;

class HttpClientTest extends TestCase
{
    public function setUp(): void
    {
        putenv('base_uri=https://google.com');
    }

    public function testSendRequest()
    {
        $httpClient = new HttpClient();
        $response = $httpClient->sendRequest('GET', '/', null, []);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
