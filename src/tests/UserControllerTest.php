<?php
use PHPUnit\Framework\TestCase;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class UserControllerTest extends TestCase
{
    /**
     * Test UserController::authAction action
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAuthAction()
    {
        $mock = new MockHandler([
            new Response(200, ['Content-Type' => 'json'], '{
                "status": "OK",
                "token": "dsfd79843r32d1d3dx23d32d"
            }
            ')
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $response = $client->request('GET', '/auth', [
            'query' => [
                'login' => 'test',
                'pass' => '12345'
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $content = $response->getBody()->getContents();
        $content = json_decode($content);
        $this->assertEquals('OK', $content->status);
        $this->assertEquals('dsfd79843r32d1d3dx23d32d', $content->token);
    }

    /**
     * Test UserController::getAction action
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testGetAction()
    {
        $mock = new MockHandler([
            new Response(200, ['Content-Type' => 'json'], '{
                "status": "OK",
                "active": "1",
                "blocked": false,
                "created_at": 1587457590,
                "id": 23,
                "name": "Ivanov Ivan",
                "permissions": [
                    {
                        "id": 1,
                        "permission": "comment"
                    },
                    {
                        "id": 2,
                        "permission": "upload photo"
                    },
                    {
                        "id": 3,
                        "permission": "add event"
                    }
                ]
            }'
        )
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $response = $client->request('GET', '/get-user/ivanov', [
            'query' => [
                'token' => 'dsfd79843r32d1d3dx23d32d',
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $content = $response->getBody()->getContents();
        $content = json_decode($content);
        $this->assertEquals('OK', $content->status);
        $this->assertEquals('Ivanov Ivan', $content->name);
        $this->assertEquals(1, $content->active);
        $this->assertEquals(false, $content->blocked);

        $this->assertEquals('comment', $content->permissions[0]->permission);
        $this->assertEquals('upload photo', $content->permissions[1]->permission);
        $this->assertEquals('add event', $content->permissions[2]->permission);
    }

    /**
     * Test UserController::updateAction action
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testUpdateAction()
    {
        $mock = new MockHandler([
            new Response(200, ['Content-Type' => 'json'], '{
                "status": "OK"
            }
            ')
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $jsonData = '{
            "active": "1",
            "blocked": true,
            "name": "Petr Petrovich",
            "permissions": [
                {
                    "id": 1,
                    "permission": "comment"
                },
            ]
        }
        ';
        $jsonData = json_decode($jsonData);

        $response = $client->request('POST', '/user/5/update', [
            'json' => $jsonData
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $content = $response->getBody()->getContents();
        $content = json_decode($content);

        $this->assertEquals('OK', $content->status);
    }
}
