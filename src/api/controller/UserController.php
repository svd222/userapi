<?php
/**
 * Created by PhpStorm
 * Author: Evgeny Berezhnoy <svd22286@gmail.com>
 * Date: 21/12/2020
 * Time: 19:27
 */

namespace App\Api\Controller;

use App\Component\HttpClient;

/**
 * Class UserController
 * @package App\Api
 */
class UserController
{
    /**
     * Provides /auth route
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function authAction()
    {
        $client = new HttpClient();

        /*$login = getenv('login');
        $pass = getenv('pass');

        $jsonData = '{
            "login": "' . $login . '",
            "pass": "' . $pass . '"
        }
        ';

        $jsonData = json_encode($jsonData);*/

        $response = $client->sendRequest('GET', '/', null); // replace 3`nd param by $jsonData to send real auth authorization data

        if ($response->getStatusCode() == 200) {
            return json_encode([
                'token' => 'dsfd79843r32d1d3dx23d32d'
            ]);

            //!!! uncomment following to get real response

            /*$body = $response->getBody();
            $content = json_decode($body->getContents());
            switch ($body->status) {
                case 'OK': {
                    return json_encode([
                        //'token' => $content->token
                        'token' => 'dsfd79843r32d1d3dx23d32d'
                    ]);
                }
                case 'Not found': {
                    return 'User not found';
                }
                case 'Error': {
                    return 'Error';
                }
            }*/
        }
    }

    /**
     * Provides /user/<user-id>/update route
     *
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateAction()
    {
        $client = new HttpClient();

        //!!! Uncomment code below to dynamically receive incoming request

        /*$uri = parse_url($_SERVER['REQUEST_URI']);
        $path = $uri['path'];
        $userId = $path[1];

        $token = $path['query'];
        $token = explode('&', $token);
        $token = $token[0];
        $token = explode('=', $token);
        $token = $token[1];*/

        $token = 'dsfd79843r32d1d3dx23d32d';
        $userId = 5;

        $path = 'user' . DIRECTORY_SEPARATOR . $userId . DIRECTORY_SEPARATOR . 'update';

//        $jsonData = file_get_contents("php://input");

        $jsonData = '{
            "active": "1",
            "blocked": true,
            "name": "Petr Petrovich",
            "permissions": [
                {
                    "id": 1,
                    "permission": "comment"
                },
            ]
        }
        ';
        $jsonData = json_decode($jsonData);

        $response = $client->sendRequest('POST', $path, $jsonData, [ // user/<user-id>/update?token=<token>
            'token' => $token
        ]);

        if ($response->getStatusCode() == 200) {
            $content =  $response->getBody()->getContents();
            $content = json_decode($content);
            switch ($content->status) {
                case 'OK': {
                    return json_encode(new class {
                        public $status;

                        public function __contstruct() {
                            $this->status = 'OK';
                        }
                    });
                }
                case 'Not found': {
                    return 'User not found';
                }
                case 'Error': {
                    return 'Error';
                }
            }
        }
    }

    /**
     * Provides /get-user/<username> route
     *
     * @return mixed|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAction()
    {
        $client = new HttpClient();

        //!!! Uncomment code below to dynamically receive incoming request

        /*$uri = parse_url($_SERVER['REQUEST_URI']);
        $path = $uri['path'];
        $userName = $path[1];

        $token = $path['query'];
        $token = explode('&', $token);
        $token = $token[0];
        $token = explode('=', $token);
        $token = $token[1];*/

        $userName = 'ivanov';
        $token = 'dsfd79843r32d1d3dx23d32d';

        $path = 'get_user' . DIRECTORY_SEPARATOR . $userName;

        $response = $client->sendRequest('GET', $path, '{}', [ // get-user/<username>?token=<token>
            'token' => $token
        ]);

        if ($response->getStatusCode() == 200) {
            $content = $response->getBody()->getContents();
            $content = json_decode($content);
            switch ($content->status) {
                case 'OK': {
                    return $content;
                }
                case 'Not found': {
                    return 'User not found';
                }
                case 'Error': {
                    return 'Error';
                }
            }
        }
    }
}
