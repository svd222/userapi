<?php
/**
 * Created by PhpStorm
 * Author: Evgeny Berezhnoy <svd22286@gmail.com>
 * Date: 21/12/2020
 * Time: 18:11
 */

namespace App\Component;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class HttpClient
 * @package App\Component
 */
class HttpClient
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => getenv('base_uri')]);
    }

    /**
     * @param string $method
     * @param string $path
     * @param null $jsonData
     * @param array $queryParams
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function sendRequest(string $method, string $path, $jsonData = null, array $queryParams = []): ResponseInterface
    {
        return $this->client->request($method, $path, [
            'query' => $queryParams,
            'json' => $jsonData
        ]);
    }
}
