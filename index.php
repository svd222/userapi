<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require './vendor/autoload.php';

// https://github.com/stein189/Simple-PHP-Router
use Szenis\Routing\Router;

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

$router = new Router();

$router->add('/auth', 'GET', '\App\Api\Controller\UserController::authAction');

$router->add('/user/{id}/update{?:token}', 'POST', '\App\Api\Controller\UserController::updateAction');

$router->add('/get-user/{username}{?:token}', 'POST', '\App\Api\Controller\UserController::getAction');

$response = $router->resolve($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);

switch ($response['code']) {
    case \Szenis\Routing\Route::STATUS_NOT_FOUND:
        echo '404 Page not found';
        break;

    case \Szenis\Routing\Route::STATUS_FOUND:
        if ($response['handler'] instanceof \Closure) {
            echo call_user_func_array($response['handler'], $response['arguments']);
            break;
        }

        if (is_string($response["handler"])) {
            $callable = explode("::", $response['handler']);
            $controller = new $callable[0];
            $action = $callable[1];
            echo $controller->$action($response["arguments"]);
            break;
        }

        break;
}
